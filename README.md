# A Certbot installer plugin for Hashicorp Vault
The EFF’s [certbot](https://eff-certbot.readthedocs.io/en/stable/index.html)
automates the creation/renewal of TLS Certificates. `certbot` supports 3rd-party
plugins to customize the challenge and installation process. This project
provides a certbot installer plugin to upload new certificates directly into
Hashicorp’s Vault. 

# Docker
A `Dockerfile` is provided to build and install the plugin directly into the
official [certbot/certbot](https://hub.docker.com/r/certbot/certbot) docker
image. A `docker-compose.yml` is included as an example.
