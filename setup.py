from setuptools import setup, find_packages

setup(
    entry_points={
        'certbot.plugins': [
            'vault = certbot_hvac.installer:VaultInstaller',
        ]
    },
    install_requires=[
        'certbot>=1.1.0',
        'hvac>=0.10.0',
        'pyopenssl>=19.1.0',
        'setuptools'
    ],
)
