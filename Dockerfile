FROM python:3 AS builder

WORKDIR /source

# Copy our plugin
COPY ./certbot_hvac/ ./certbot_hvac
COPY ./setup.py .

# Update pip/setuptools and build our dist
RUN python -m pip install --upgrade pip setuptools
RUN python setup.py bdist_wheel

# Our main target image
FROM certbot/certbot:latest

# Copy our plugin dist from our builder
COPY --from=builder /source/dist/ /deps

# Install the plugin
RUN pip install /deps/*

ENV VAULT_URL
ENV VAULT_TOKEN
ENV VAULT_PATH

#ENTRYPOINT ["certbot", "--installer", "vault", "--domains", "example.com", "--dry-run"] 
ENTRYPOINT ["certbot", "plugins"]
