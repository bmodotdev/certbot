import os
import hvac
import OpenSSL.crypto

from certbot import errors, interfaces
from certbot.plugins import common

class VaultInstaller(common.Plugin):
    """Hashicorp Vault installer plugin."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        url = os.environ.get('VAULT_ADDR')
        token = os.environ.get('VAULT_TOKEN')
        self.client = hvac.Client(url=url, token=token)
        self.secrets_path = os.environ.get('VAULT_PATH', 'secrets/certbot')

    def prepare(self): 
        """Prepare the plugin."""
        if not self.client.is_authenticated():
            raise errors.PluginError('Failed to authenticate')

    def more_info(self): 
        """Human-readable string to help the user."""
        return 'Creates or updates a secret in Hashicorp Vault.'

    def get_all_names(self):
        """Returns all names that may be authenticated."""
        pass

    def deploy_cert(self, domain, cert_path, key_path, chain_path, fullchain_path):
        """Deploy certificate.

        Args:
            domain (str): Primary domain name of the certificate.
            cert_path (str): Path to the certificate file.
            key_path (str): Path to the private key file.
            chain_path (str): Path to the certificate chain file.
            fullchain_path (str): Path to the full certificate chain file.

        Returns:
            None

        """
        secret_path = os.path.join(self.secrets_path, domain)
        cert = OpenSSL.crypto.load_pem_x509_certificate(
            OpenSSL.crypto.FILETYPE_PEM, open(cert_path).read()
        )
        self.client.secrets.kv.create_or_update_secret(
            secret_path,
            {
                'cert': open(cert_path, 'r').read(),
                'key': open(key_path, 'r').read(),
                'chain': open(chain_path, 'r').read(),
                'fullchain': open(fullchain_path, 'r').read(),
                'serial': cert.get_serial_number(),
                'issue_date': cert.get_notBefore(),
                'expire_date': cert.get_notAfter()
            }
        )

def get_installer():
    return VaultInstaller
